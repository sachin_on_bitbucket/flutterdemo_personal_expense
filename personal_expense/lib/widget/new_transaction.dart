import 'package:flutter/material.dart';

class NewTransaction extends StatelessWidget{
//  String titleInput;
//  String amountInput;
  final titleTextEditingController = TextEditingController();
  final amountTextEditingController = TextEditingController();
  final Function addTx;

  NewTransaction(this.addTx);

  @override
  Widget build(BuildContext context) {

    return Card(
      elevation: 5,
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            TextField(
              decoration: InputDecoration(labelText: 'Title'),
//                    onChanged: (value) => titleInput = value,
              controller: titleTextEditingController,

            ),
            TextField(
              decoration: InputDecoration(labelText: 'Amount'),
//                      onChanged: (value) => amountInput = value
              controller: amountTextEditingController,
            ),
            FlatButton(
              child: Text('Add Transaction'),
              textColor: Colors.purple,
              color: Colors.grey,
              onPressed: () {
//                print (titleTextEditingController.text);
//                print (amountTextEditingController.text);
              addTx(titleTextEditingController.text, double.parse(amountTextEditingController.text));
              },
            )
          ],
        ),
      ),
    );
  }
}