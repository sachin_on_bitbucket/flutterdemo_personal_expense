import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

//import 'package:personal_expense/model/transaction.dart';
import '../model/transaction.dart';

class TransactionList extends StatelessWidget {
  List<Transaction> transactionList;

  TransactionList(this.transactionList);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 300,
      child: ListView.builder(
        itemBuilder: (ctx, index) {
          return Card(
            child: Row(
              children: <Widget>[
                Container(
                  child: Text(
                    '\$${transactionList[index].amount}',
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 20,
                        color: Colors.purple),
                  ),
                  margin: EdgeInsets.symmetric(
                    vertical: 10,
                    horizontal: 15,
                  ),
                  decoration: BoxDecoration(
                      border: Border.all(color: Colors.purple, width: 2)),
                  padding: EdgeInsets.all(10),
                ),
                Column(
                  children: <Widget>[
                    Text(
                      transactionList[index].title,
                      style:
                      TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                    ),
//                        Text(DateFormat('yyyy/MM/dd hh:mm a').format(tx.date), style: TextStyle(color: Colors.blueGrey),)
                    Text(
                      DateFormat.yMMMd().format(transactionList[index].date),
                      style: TextStyle(color: Colors.blueGrey),
                    )
                  ],
                  crossAxisAlignment: CrossAxisAlignment.start,
                )
              ],
            ),
          );
        },
        itemCount: transactionList.length,
        /*children: transactionList.map((tx) {

        }).toList(),*/
      ),
    );
  }
}
