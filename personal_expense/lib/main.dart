import 'package:flutter/material.dart';
import 'package:personal_expense/widget/user_transactions.dart';
import './widget/new_transaction.dart';
import './widget/transaction_list.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
      appBar: AppBar(
        title: Text('Pesonal Expense'),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
              child: Card(
                child: Container(
                  child: Text('CHART!'),
                  margin: EdgeInsets.fromLTRB(10, 5, 10, 5),
                ),
                color: Colors.amber,
                margin: EdgeInsets.fromLTRB(10, 5, 10, 5),
              ),
              width: double.infinity,
              color: Colors.blue,
            ),
            UserTransactions()
          ],
//        mainAxisAlignment: MainAxisAlignment.spaceAround,
          crossAxisAlignment: CrossAxisAlignment.center,
        ),
      ),
    ));
  }
}
